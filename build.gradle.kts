@file:Suppress("HardCodedStringLiteral")

plugins {
    kotlin("jvm") version "2.1.0" apply true
    id("java-library")
    kotlin("plugin.serialization") version "2.1.0"
}

kotlin {
    explicitApi() // for strict mode
//    explicitApiWarning() // warning mode
}

group = "de.hdc.tools"
version = "2025-02-17"

kotlin {
    jvmToolchain(libs.versions.jvmToolchain.get().toInt())
}

dependencies {
    testImplementation(libs.bundles.testing)

    implementation(kotlin("reflect"))
    implementation(libs.kotlin.coroutines)
    implementation(libs.kotlin.serializationJson)
}
