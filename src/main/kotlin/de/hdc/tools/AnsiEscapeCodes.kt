@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

// Regular foreground colors
public const val ANSI_RESET: String = "\u001B[0m"
public const val ANSI_BLACK: String = "\u001B[30m"
public const val ANSI_RED: String = "\u001B[31m"
public const val ANSI_GREEN: String = "\u001B[32m"
public const val ANSI_YELLOW: String = "\u001B[33m"
public const val ANSI_BLUE: String = "\u001B[34m"
public const val ANSI_PURPLE: String = "\u001B[35m"
public const val ANSI_CYAN: String = "\u001B[36m"
public const val ANSI_WHITE: String = "\u001B[37m"

// High intensity foreground colors
public const val BLACK_BRIGHT: String = "\u001b[0;90m" // BLACK
public const val RED_BRIGHT: String = "\u001b[0;91m" // RED
public const val GREEN_BRIGHT: String = "\u001b[0;92m" // GREEN
public const val YELLOW_BRIGHT: String = "\u001b[0;93m" // YELLOW
public const val BLUE_BRIGHT: String = "\u001b[0;94m" // BLUE
public const val PURPLE_BRIGHT: String = "\u001b[0;95m" // PURPLE
public const val CYAN_BRIGHT: String = "\u001b[0;96m" // CYAN
public const val WHITE_BRIGHT: String = "\u001b[0;97m" // WHITE
