@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

import java.util.*

@JvmInline
/**
 * A string representation of a hex value.
 */
public value class HexString private constructor(public val value: String) {
    init {
        require(value.isNotEmpty()) { "String can not be empty!" }
        require(
            value.matches(Regex("""^(([0-9a-f]){1,4}([ -:])*)+$"""))
        ) { "Invalid hex string <$value>!" }
    }

    public companion object {
        /**
         * Create a string representation of a hex value.
         */
        public operator fun invoke(value: String?): HexString {
            require(value != null) { "Hex string cannot be null!" }
            return HexString(value.lowercase(Locale.UK))
        }
    }

    /**
     * Converts a string of hexadecimal to a ByteArray
     */
    public fun toByteArray(): ByteArray {
        val hex = value.lowercase()
            .replace(" ", "")
            .replace(":", "")
            .replace("-", "")
        val len = hex.length
        val result = ByteArray(len / 2)
        (0 until len step 2).forEach { i ->
            result[i.shr(1)] =
                HEX_CHARS.indexOf(hex[i]).shl(4).or(
                    HEX_CHARS.indexOf(hex[i + 1])
                ).toByte()
        }
        return result
    }
}

/**
 * Convert a ByteArray to a hex string.
 * Multipaltform version.
 */
public fun ByteArray.toHexString(): String =
    joinToString("") { (0xFF and it.toInt()).toString(16).padStart(2, '0') }
        .reversed().mapIndexed { i, c ->
            if ((i > 0) && (i % 4 == 0)) {
                " $c"
            } else {
                c
            }
        }.joinToString("").reversed()

private const val HEX_CHARS = "0123456789abcdef"
