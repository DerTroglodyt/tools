@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.*
import java.util.logging.Formatter

/**
 * Simplifies and beautifies Logging by extending Any with logging functions.
 *
 * Example:
 * useNiceLogging()
 * logWarning { "Something went wrong!" }
 */

/**
 * Use a nicer Formatter for logging. Produces single lines in the form of:
 * 2019-02-23 12:01:33 LEVEL LoggerName Message
 */
public fun useNiceLogging(logLevel: Level = Level.WARNING): Logger {
    val rootLogger = LogManager.getLogManager().getLogger("")
    rootLogger.level = logLevel
    for (h in rootLogger.handlers) {
        h.level = logLevel
        h.formatter = NiceLogFormatter()
    }
    return rootLogger
}

public fun getClassName(javaClass: Class<*>): String {
    return if (javaClass.kotlin.isCompanion) javaClass.enclosingClass.name + "$" + javaClass.simpleName
    else javaClass.name
}

/**
 * Get a Logger for this class.
 */
public inline fun <reified R : Any> R.logger(): Logger {
    return Logger.getLogger(getClassName(this.javaClass))
}

/**
 * Log a debug message with hyperlink to source code line.
 */
public inline fun <reified R : Any> R.logDebug(s: () -> String) {
    with(logger()) {
        if (isLoggable(Level.FINEST)) finest(s.invoke())
    }
}

/**
 * Log an informational message.
 */
public inline fun <reified R : Any> R.logInfo(s: () -> String) {
    with(logger()) {
        if (isLoggable(Level.INFO)) info(s.invoke())
    }
}

/**
 * Log a warning message.
 */
public inline fun <reified R : Any> R.logWarning(s: () -> String) {
    with(logger()) {
        if (isLoggable(Level.WARNING)) warning(s.invoke())
    }
}

/**
 * Log an error message with hyperlink to source code line and small stack trace.
 */
public inline fun <reified R : Any> R.logError(s: () -> String) {
    with(logger()) {
        if (isLoggable(Level.SEVERE)) severe(s.invoke())
    }
}

/**
 * More readable log formatting in the form of:
 * 2019-02-23 12:01:33 LEVEL LoggerName Message
 */
private class NiceLogFormatter : Formatter() {
    override fun format(record: LogRecord): String {
        val c = when (record.level) {
            Level.FINEST, Level.FINER, Level.FINE -> {
                "\u001b[38;5;142m"
            }

            Level.INFO -> {
                "\u001b[38;5;253m"
            }

            Level.SEVERE -> {
                "\u001b[38;5;197m"
            }

            else -> {
                "\u001b[31m"
            }
        }
        val d = UTCDateFormat.format(Date(record.millis))
        val l = getCallLocation()
        val s = "$c $d ${getLevel(record.level)} ${record.message}   " +
                "\u001b[38;5;238m@${record.sourceClassName}(${l.fileName}:${l.lineNumber})\u001b[0m\n"
        return addStackTrace(record, s)
    }

    companion object {
        @JvmStatic
        @Transient
        private val UTCDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK)
    }

    private fun getLevel(level: Level): String {
        return when (level) {
            Level.OFF -> "OFF  "
            Level.ALL -> "ALL  "
            Level.CONFIG -> "DEBUG"
            Level.FINE -> "DEBUG"
            Level.FINER -> "DEBUG"
            Level.FINEST -> "DEBUG"
            Level.INFO -> "INFO "
            Level.WARNING -> "WARN "
            Level.SEVERE -> "FATAL"
            else -> "?????"
        }
    }

    private fun addStackTrace(record: LogRecord, s: String): String {
        var s1 = s
        if (record.level === Level.SEVERE) {
            getStackTrace().take(5).forEach { s1 += "   at $it\n" }
        }
        return s1
    }

    private fun getStackTrace(): Sequence<StackTraceElement> {
        return Exception("Not named").stackTrace.asSequence()
            .filter {
                (!it.toString().contains("java.util.logging")) &&
                        (!it.toString().contains("LoggingKt")) &&
                        (!it.toString().contains("NiceLogFormatter"))
            }
    }

    private fun getCallLocation(): StackTraceElement = getStackTrace().first()
}
