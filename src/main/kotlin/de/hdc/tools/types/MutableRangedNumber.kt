package de.hdc.tools.types

import de.hdc.tools.math.*
import kotlin.reflect.KProperty

/**
 * Holds a value which is guarantied to lie in a fixed range.
 * Can be used as a delegate:
 *   val d: Double by RangedDouble(0.0)
 */
public interface MutableRangedNumber<T> :
    RangedNumber<T>,
    Comparable<RangedNumber<T>>
        where T : Number, T : Comparable<T> {
    public override var value: T
    public override operator fun unaryMinus(): MutableRangedNumber<T>
    public override operator fun unaryPlus(): MutableRangedNumber<T>
    public override operator fun plus(x: Number): MutableRangedNumber<T>
    public override operator fun minus(x: Number): MutableRangedNumber<T>
    public override operator fun times(x: Number): MutableRangedNumber<T>
    public override operator fun div(x: Number): MutableRangedNumber<T>
    public override operator fun rem(x: Number): MutableRangedNumber<T>
    public operator fun plusAssign(value: T)
    public operator fun minusAssign(value: T)
    public operator fun timesAssign(value: T)
    public operator fun divAssign(value: T)
    public operator fun remAssign(value: T)
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 * Can be used as a delegate:
 *   val d: Double by RangedDouble(0.0)
 */
public abstract class AbstractMutableRangedNumber<T>(
    initialValue: T,
    /** The inclusive interval in which the value is guarantied to lie. */
    public val range: ClosedRange<T>
) : MutableRangedNumber<T>,
    Number(),
    Comparable<RangedNumber<T>>
        where T : Number, T : Comparable<T> {
    override var value: T = initialValue.clamp(range)
        set(value) {
            field = value.clamp(range)
        }

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value
    public operator fun setValue(
        thisRef: Any?,
        property: KProperty<*>,
        newValue: T
    ) {
        value = newValue
    }

    override operator fun unaryPlus(): AbstractMutableRangedNumber<T> = this

    override fun toByte(): Byte = value.toInt().toByte()
    @Deprecated(
        "Direct conversion to Char is deprecated. Use toInt().toChar() or Char constructor instead.\nIf you override toChar() function in your Number inheritor, it's recommended to gradually deprecate the overriding function and then remove it.\nSee https://youtrack.jetbrains.com/issue/KT-46465 for details about the migration",
        replaceWith = ReplaceWith("this.toInt().toChar()")
    )
    override fun toChar(): Char = value.toInt().toChar()
    override fun toDouble(): Double = value.toDouble()
    override fun toFloat(): Float = value.toFloat()
    override fun toInt(): Int = value.toInt()
    override fun toLong(): Long = value.toLong()
    override fun toShort(): Short = value.toLong().toShort()

    override fun compareTo(other: RangedNumber<T>): Int = value.compareTo(other.value)
    override fun hashCode(): Int = value.hashCode()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RangedNumber<*>

        return value == other.value
    }
}

public class MutableRangedFloat(initialValue: Float, range: ClosedRange<Float>) :
    AbstractMutableRangedNumber<Float>(initialValue, range) {
    override operator fun unaryMinus(): MutableRangedFloat = times(-1f)
    override operator fun plus(x: Number): MutableRangedFloat = MutableRangedFloat(value + x.toFloat(), range)
    override operator fun minus(x: Number): MutableRangedFloat = MutableRangedFloat(value - x.toFloat(), range)
    override operator fun times(x: Number): MutableRangedFloat = MutableRangedFloat(value * x.toFloat(), range)
    override operator fun div(x: Number): MutableRangedFloat = MutableRangedFloat(value / x.toFloat(), range)
    override operator fun rem(x: Number): MutableRangedFloat = MutableRangedFloat(value % x.toFloat(), range)
    override operator fun plusAssign(value: Float) {
        this.value += value
    }

    override operator fun minusAssign(value: Float) {
        this.value -= value
    }

    override operator fun timesAssign(value: Float) {
        this.value *= value
    }

    override operator fun divAssign(value: Float) {
        this.value /= value
    }

    override operator fun remAssign(value: Float) {
        this.value %= value
    }
}

public class MutableRangedDouble(initialValue: Double, range: ClosedRange<Double>) :
    AbstractMutableRangedNumber<Double>(initialValue, range) {
    override operator fun unaryMinus(): MutableRangedDouble = times(-1f)
    override operator fun plus(x: Number): MutableRangedDouble = MutableRangedDouble(value + x.toDouble(), range)
    override operator fun minus(x: Number): MutableRangedDouble = MutableRangedDouble(value - x.toDouble(), range)
    override operator fun times(x: Number): MutableRangedDouble = MutableRangedDouble(value * x.toDouble(), range)
    override operator fun div(x: Number): MutableRangedDouble = MutableRangedDouble(value / x.toDouble(), range)
    override operator fun rem(x: Number): MutableRangedDouble = MutableRangedDouble(value % x.toDouble(), range)
    override operator fun plusAssign(value: Double) {
        this.value += value
    }

    override operator fun minusAssign(value: Double) {
        this.value -= value
    }

    override operator fun timesAssign(value: Double) {
        this.value *= value
    }

    override operator fun divAssign(value: Double) {
        this.value /= value
    }

    override operator fun remAssign(value: Double) {
        this.value %= value
    }
}

public class MutableRangedShort(initialValue: Short, range: ClosedRange<Short>) :
    AbstractMutableRangedNumber<Short>(initialValue, range) {
    override operator fun unaryMinus(): MutableRangedShort = times(-1f)
    override operator fun plus(x: Number): MutableRangedShort = MutableRangedShort((value + x.toInt()).toShort(), range)
    override operator fun minus(x: Number): MutableRangedShort = MutableRangedShort((value - x.toInt()).toShort(), range)
    override operator fun times(x: Number): MutableRangedShort = MutableRangedShort((value * x.toInt()).toShort(), range)
    override operator fun div(x: Number): MutableRangedShort = MutableRangedShort((value / x.toInt()).toShort(), range)
    override operator fun rem(x: Number): MutableRangedShort = MutableRangedShort((value % x.toInt()).toShort(), range)
    override operator fun plusAssign(value: Short) {
        this.value = (this.value + value).toShort()
    }

    override operator fun minusAssign(value: Short) {
        this.value = (this.value - value).toShort()
    }

    override operator fun timesAssign(value: Short) {
        this.value = (this.value * value).toShort()
    }

    override operator fun divAssign(value: Short) {
        this.value = (this.value / value).toShort()
    }

    override operator fun remAssign(value: Short) {
        this.value = (this.value % value).toShort()
    }
}

public class MutableRangedInt(initialValue: Int, range: ClosedRange<Int>) :
    AbstractMutableRangedNumber<Int>(initialValue, range) {
    override operator fun unaryMinus(): MutableRangedInt = times(-1f)
    override operator fun plus(x: Number): MutableRangedInt = MutableRangedInt(value + x.toInt(), range)
    override operator fun minus(x: Number): MutableRangedInt = MutableRangedInt(value - x.toInt(), range)
    override operator fun times(x: Number): MutableRangedInt = MutableRangedInt(value * x.toInt(), range)
    override operator fun div(x: Number): MutableRangedInt = MutableRangedInt(value / x.toInt(), range)
    override operator fun rem(x: Number): MutableRangedInt = MutableRangedInt(value % x.toInt(), range)
    override operator fun plusAssign(value: Int) {
        this.value += value
    }

    override operator fun minusAssign(value: Int) {
        this.value -= value
    }

    override operator fun timesAssign(value: Int) {
        this.value *= value
    }

    override operator fun divAssign(value: Int) {
        this.value /= value
    }

    override operator fun remAssign(value: Int) {
        this.value %= value
    }
}

public class MutableRangedLong(initialValue: Long, range: ClosedRange<Long>) :
    AbstractMutableRangedNumber<Long>(initialValue, range) {
    override operator fun unaryMinus(): MutableRangedLong = times(-1f)
    override operator fun plus(x: Number): MutableRangedLong = MutableRangedLong(value + x.toLong(), range)
    override operator fun minus(x: Number): MutableRangedLong = MutableRangedLong(value - x.toLong(), range)
    override operator fun times(x: Number): MutableRangedLong = MutableRangedLong(value * x.toLong(), range)
    override operator fun div(x: Number): MutableRangedLong = MutableRangedLong(value / x.toLong(), range)
    override operator fun rem(x: Number): MutableRangedLong = MutableRangedLong(value % x.toLong(), range)
    override operator fun plusAssign(value: Long) {
        this.value += value
    }

    override operator fun minusAssign(value: Long) {
        this.value -= value
    }

    override operator fun timesAssign(value: Long) {
        this.value *= value
    }

    override operator fun divAssign(value: Long) {
        this.value /= value
    }

    override operator fun remAssign(value: Long) {
        this.value %= value
    }
}
