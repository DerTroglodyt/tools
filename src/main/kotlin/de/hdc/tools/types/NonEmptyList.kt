package de.hdc.tools.types

@JvmInline
public value class NonEmptyList<out E>(public val value: List<E>) {
    init {
        require(value.isNotEmpty())
    }

    public constructor(vararg elements: E) : this(elements.toList())
}

public fun <E> List<E>.toNonEmptyList(): NonEmptyList<E> {
    return NonEmptyList(this)
}
