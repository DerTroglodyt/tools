package de.hdc.tools.types

/**
 * Encapsulates the result of a function call.
 * Can be a success or failure which can be actioned upon accordingly.
 * Failures are not restricted to [Throwable]s as in the Kotlin standard lib implementation of [Result].
 */
public sealed interface Returned<out RESULT, out FAILURE> {
    /**
     * Returns the [Returned] on success or null.
     */
    public fun getOrNull(): RESULT?

    /**
     * Returns the [Returned] on success or throws an Exception on failure.
     * If the failure already was of type [Throwable] it is rethrown.
     * Otherwise a generic Exception with the result.toString() as message is thrown.
     */
    public fun getOrThrow(): RESULT

    /**
     * Return the failure or null.
     */
    public fun failureOrNull(): FAILURE?

    /**
     * Executes one of the given blocks depending on Returned being a success or a failure.
     */
    public fun <R> fold(onSuccess: (RESULT) -> R, onFailure: (FAILURE) -> R): R

    /**
     * Executes the given block if the Returned is a success.
     * Returns the Returned for chaining calls fluently.
     */
    public fun onSuccess(action: (RESULT) -> Unit): Returned<RESULT, FAILURE>

    /**
     * Executes the given block if the Returned is a failure.
     * Returns the Returned for chaining calls fluently.
     */
    public fun onFailure(action: (FAILURE) -> Unit): Returned<RESULT, FAILURE>

    public companion object {
        /**
         * Create a Returned that is a success.
         */
        public fun <RESULT> success(result: RESULT): Returned<RESULT, Any?> = Success(result)

        /**
         * Create a Returned that is a failure.
         */
        public fun <FAILURE> failure(error: FAILURE): Returned<Any?, FAILURE> = Failure(error)
    }
}

public data class Success<out RESULT, out FAILURE>(val value: RESULT) : Returned<RESULT, FAILURE> {
    override fun equals(other: Any?): Boolean = (other is Success<*, *>) && (value == other.value)
    override fun hashCode(): Int = value.hashCode()
    override fun toString(): String = "Success($value)"
    override fun getOrNull(): RESULT? = value
    override fun getOrThrow(): RESULT = value
    override fun failureOrNull(): FAILURE? = null
    override fun <R> fold(onSuccess: (RESULT) -> R, onFailure: (FAILURE) -> R): R = onSuccess(value)
    override fun onSuccess(action: (RESULT) -> Unit): Returned<RESULT, FAILURE> = action(value).let { this }
    override fun onFailure(action: (FAILURE) -> Unit): Returned<RESULT, FAILURE> = this
}

public data class Failure<out RESULT, out FAILURE>(val failure: FAILURE) : Returned<RESULT, FAILURE> {
    override fun equals(other: Any?): Boolean = (other is Failure<*, *>) && (failure == other.failure)
    override fun hashCode(): Int = failure.hashCode()
    override fun toString(): String = "Failure($failure)"
    override fun getOrNull(): RESULT? = null

    override fun getOrThrow(): RESULT {
        when (failure) {
            is Throwable -> throw failure
            else -> throw Exception(failure.toString())
        }
    }

    override fun failureOrNull(): FAILURE = failure
    override fun <R> fold(onSuccess: (RESULT) -> R, onFailure: (FAILURE) -> R): R = onFailure(failure)
    override fun onSuccess(action: (RESULT) -> Unit): Returned<RESULT, FAILURE> = this
    override fun onFailure(action: (FAILURE) -> Unit): Returned<RESULT, FAILURE> = action(failure).let { this }
}
