package de.hdc.tools.types

import kotlinx.serialization.Serializable
import java.security.*
import kotlin.text.toHexString

/**
 * An immutable unique ID.
 *
 * Generically typed for ease of use:
 *
 *     data class User(val id: Id<User>)
 *     data class Address(val id: Id<Address>)
 */
@Suppress("HardCodedStringLiteral")
@Serializable
public class Id<out T> private constructor(private val id: ByteArray) {
    /**
     * Validate on deserialization.
     */
    init {
        require(id.size == BYTES) { "Id ByteArray size must be $BYTES but was ${id.size} bytes!" }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (other !is Id<*>) return false

        return id.contentEquals(other.id)
    }

    override fun hashCode(): Int {
        return id.contentHashCode()
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun toString(): String = "Id(${id.toHexString()})"

    public companion object {
        /**
         * Create a new random ID.
         */
        public operator fun <T> invoke(): Id<T> {
            val bytes = ByteArray(BYTES)
            rng.nextBytes(bytes)
            return Id(bytes)
        }

        private const val BYTES = 16
        private val rng: SecureRandom = SecureRandom()
    }
}
