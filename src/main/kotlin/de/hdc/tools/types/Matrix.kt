package de.hdc.tools.types

/**
 * Constructs a 2 dimensional array of T.
 */
public class Matrix<T : Any?>(
    internal val maxX: Int,
    internal val maxY: Int,
    init: (Int, Int) -> T
) {
    @Suppress("HardCodedStringLiteral")
    private val NULL_STR = "<null>"

    public var maxLen: Int = 0
        private set
    private val map: List<MutableList<T>> = (0..maxY).map { y ->
        (0..maxX).map { x -> init(x, y) }.toMutableList()
    }

    public operator fun get(x: Int, y: Int): T = map[y][x]
    public operator fun set(x: Int, y: Int, newValue: T) {
        map[y][x] = newValue
        val nl = (newValue?.toString() ?: NULL_STR).length
        if (nl > maxLen) maxLen = nl
    }

    public operator fun get(pos: IntPosition2D): T = map[pos.y][pos.x]
    public operator fun set(pos: IntPosition2D, newValue: T) {
        map[pos.y][pos.x] = newValue
        val nl = (newValue?.toString() ?: NULL_STR).length
        if (nl > maxLen) maxLen = nl
    }

    public fun columnIndices(): IntRange = 0..maxX
    public fun rowIndices(): IntRange = 0..maxY

    public fun row(y: Int): List<T> {
        return columnIndices().map { x -> map[y][x] }
    }

    public fun column(x: Int): List<T> {
        return rowIndices().map { y -> map[y][x] }
    }

    public val pos: List<IntPosition2D> by lazy {
        (0..maxY).flatMap { y ->
            (0..maxX).map { x ->
                IntPosition2D(x, y)
            }
        }
    }

    public fun values(): List<T> = pos.map { this[it] }

    public fun filterValue(f: (T) -> Boolean): List<T> =
        map.flatMap { row ->
            row.map {
                if (f(it)) it else null
            }
        }.filterNotNull()

    public fun filter(f: (T) -> Boolean): List<IntPosition2D> =
        map.flatMapIndexed { y, row ->
            row.mapIndexed { x, data ->
                if (f(data)) IntPosition2D(x, y) else null
            }
        }.filterNotNull()

    public fun forEach(f: (T) -> Unit) {
        map.forEach { row ->
            row.forEach { data ->
                f(data)
            }
        }
    }

    public fun forEachIndexed(f: (x: Int, y: Int, data: T) -> Unit) {
        map.forEachIndexed { y, row ->
            row.forEachIndexed { x, data ->
                f(x, y, data)
            }
        }
    }

    override fun toString(): String {
        val ml = maxLen + 1
        return map.joinToString("\n") { row ->
            row.joinToString(", ") { c -> (c?.toString() ?: NULL_STR).padStart(ml, ' ') }
        }
    }

    public fun toString(delimiter: String): String =
        map.joinToString("\n") { row ->
            row.joinToString(delimiter) { c -> c.toString() }
        }

    public fun toHighlightString(highlight: List<IntPosition2D>): String =
        map.mapIndexed { y, row ->
            row.mapIndexed { x, c ->
                if (IntPosition2D(x, y) in highlight) "($c)" else " $c "
            }.joinToString("")
        }.joinToString("\n")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        if (other !is Matrix<*>) return false

        if (maxX != other.maxX) return false
        if (maxY != other.maxY) return false
        if (values() != other.values()) return false

        return true
    }

    override fun hashCode(): Int {
        var result = values().hashCode()
        result = 31 * result + maxX
        result = 31 * result + maxY
        return result
    }

    public fun neighboursOrtho(x: Int, y: Int): List<IntPosition2D> {
        val list = mutableListOf<IntPosition2D>()
        if (x > 0) list.add(IntPosition2D(x - 1, y))
        if (x < maxX) list.add(IntPosition2D(x + 1, y))
        if (y > 0) list.add(IntPosition2D(x, y - 1))
        if (y < maxY) list.add(IntPosition2D(x, y + 1))
        return list
    }

    public fun neighboursOrtho(pos: IntPosition2D): List<IntPosition2D> = neighboursOrtho(pos.x, pos.y)

    public fun neighboursOrthoDiag(x: Int, y: Int): List<IntPosition2D> {
        val list = mutableListOf<IntPosition2D>()
        list += neighboursOrtho(x, y)
        if ((x > 0) && (y > 0)) list.add(IntPosition2D(x - 1, y - 1))
        if ((x < maxX) && (y > 0)) list.add(IntPosition2D(x + 1, y - 1))
        if ((x > 0) && (y < maxY)) list.add(IntPosition2D(x - 1, y + 1))
        if ((x < maxX) && (y < maxY)) list.add(IntPosition2D(x + 1, y + 1))
        return list
    }

    public fun neighboursOrthoDiag(pos: IntPosition2D): List<IntPosition2D> = neighboursOrthoDiag(pos.x, pos.y)
}
