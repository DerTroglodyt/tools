package de.hdc.tools.types

import kotlin.math.*
import kotlin.random.*

public data class IntPosition2D(val x: Int, val y: Int) : Comparable<IntPosition2D> {

    public operator fun plus(other: IntPosition2D): IntPosition2D = IntPosition2D(x + other.x, y + other.y)
    public operator fun minus(other: IntPosition2D): IntPosition2D = IntPosition2D(x - other.x, y - other.y)

    public fun manhattenDist(other: IntPosition2D): Int = abs(other.x - x) + abs(other.y - y)

    public fun neighboursOrtho(): List<IntPosition2D> {
        val list = mutableListOf<IntPosition2D>()
        list.add(IntPosition2D(x - 1, y))
        list.add(IntPosition2D(x + 1, y))
        list.add(IntPosition2D(x, y - 1))
        list.add(IntPosition2D(x, y + 1))
        return list
    }

    public fun neighboursOrthoDiag(): List<IntPosition2D> {
        val list = mutableListOf<IntPosition2D>()
        list += neighboursOrtho()
        list.add(IntPosition2D(x - 1, y - 1))
        list.add(IntPosition2D(x + 1, y - 1))
        list.add(IntPosition2D(x - 1, y + 1))
        list.add(IntPosition2D(x + 1, y + 1))
        return list
    }

    public companion object {
        public fun fromStr(s: String): IntPosition2D {
            val (x, y) = s.split(",")
            return IntPosition2D(x.toInt(), y.toInt())
        }

        public fun createRnd(width: Int, height: Int): IntPosition2D {
            return IntPosition2D(Random.nextInt(width), Random.nextInt(height))
        }

        public fun createRnd(range: IntRange): IntPosition2D {
            val w = range.last - range.first + 1
            return IntPosition2D(Random.nextInt(w) + range.first, Random.nextInt(w) + range.first)
        }
    }

    override fun compareTo(other: IntPosition2D): Int {
        return when {
            (x == other.x) && (y == other.y) -> 0
            (x < other.x) || (y < other.y) -> -1
            else -> 1
        }
    }
}
