package de.hdc.tools.types

public data class IntRectangle2D(
    val x: Int,
    val y: Int,
    val w: Int,
    val h: Int,
    private val x2: Int = x + w,
    private val y2: Int = y + h
) {
    public fun contains(pos: IntPosition2D): Boolean = (pos.x >= x) &&
            (pos.x < x2) &&
            (pos.y >= y) &&
            (pos.y < y2)

    public fun intersects(rect: IntRectangle2D): Boolean = !((rect.x > x2) ||
            (rect.x2 <= x) ||
            (rect.y > y2) ||
            (rect.y2 <= y)
            )
}
