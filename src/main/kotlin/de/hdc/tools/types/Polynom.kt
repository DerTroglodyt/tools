package de.hdc.tools.types

import kotlin.math.pow

public data class Polynom(private val map: Map<UInt, Double>) {
    public operator fun get(power: UInt): Double = map[power] ?: 0.0

    public val powers: Set<UInt> = map.keys

    public val derivative: Polynom by lazy {
        Polynom(map.mapNotNull { (p, c) ->
            if (p == 0u) null
            else {
                val dp = p - 1u
                if (dp < 0u) null
                else dp to c * p.toDouble()
            }
        }.toMap())
    }

    public fun derivativeAt(x: Double): Double =
        derivative.map.entries.sumOf { (p, c) ->
            c * x.pow(p.toInt())
        }

    override fun toString(): String {
        return "Polynom(${map.entries.joinToString(" + ") { (p, c) -> "${c}x^$p" }})"
    }
}
