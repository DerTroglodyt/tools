package de.hdc.tools.comp

import de.hdc.tools.types.*
import de.hdc.tools.types.Returned.*

public interface MachineState
public abstract class MachineTransition<S : MachineState>(public val from: S, public val to: S)
public abstract class MachineEvent<S : MachineState, T : MachineTransition<S>>(
    public val transition: T,
    public val undo: MachineEvent<S, T>? = null,
    public val canUndo: Boolean = true
)

public class StateMachine<S : MachineState, T : MachineTransition<S>, E : MachineEvent<S, T>>(
    public val state: S,
    private val events: List<E> = listOf()
) {

    public fun getEvents(): List<MachineEvent<S, T>> = events.toList()

    public fun on(event: E): Returned<StateMachine<S, T, E>, String> {
        return if (state != event.transition.from) {
            Failure("Event tried to transition from '${event.transition.from}' but actual state was '$state'!")
        } else {
            Success(StateMachine(event.transition.to, events + event))
        }
    }

    public fun undo(): Returned<StateMachine<S, T, E>, String> {
        val event = events.lastOrNull()
            ?: return Failure("No event remaining to be undone!")
        if (!event.canUndo) {
            return Failure("Last event '${event}' can't be undone!")
        }
        synchronized(events) {
            return if (event.undo != null) {
                on(event.undo as E)
            } else {
                var sm: StateMachine<S, T, E> = StateMachine(events.first().transition.from)
                try {
                    events.dropLast(1)
                        .forEach { e ->
                            sm = sm.on(e).getOrThrow()
                        }
                    Success(sm)
                } catch (e: Exception) {
                    Failure(e.message!!)
                }
            }
        }
    }
}
