package de.hdc.tools.comp

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.shareIn
import kotlin.time.Duration

/**
 * Maintains a pool of expensive connections as reusable [Flow]s.
 * Notice that, thanks to the fact we’re using WhileSubscribed, a connection will only be maintained when
 * there is at least a single coroutine using it.
 *
 * *Usage example:*
 *
 *     private val scope = CoroutineScope(SupervisorJob())
 *     private val messageConnections =
 *         ConnectionPool(scope) { threadId: String ->
 *             api.observeMessageThread(threadId)
 *         }
 *
 *     fun observeMessageThread(threadId: String) = messageConnections.getConnection(threadId)
 */
public class ConnectionPool<K, V>(
    private val scope: CoroutineScope,
    private val replay: Int = 0,
    private val stopTimeout: Duration,
    private val replayExpiration: Duration,
    private val builder: (K) -> Flow<V>,
) {
    private val connections = mutableMapOf<K, Flow<V>>()

    public fun getConnection(key: K): Flow<V> = synchronized(this) {
        connections.getOrPut(key) {
            builder(key).shareIn(
                scope,
                started = SharingStarted.WhileSubscribed(
                    stopTimeoutMillis = stopTimeout.inWholeMilliseconds,
                    replayExpirationMillis = replayExpiration.inWholeMilliseconds,
                ),
                replay = replay,
            )
        }
    }
}
