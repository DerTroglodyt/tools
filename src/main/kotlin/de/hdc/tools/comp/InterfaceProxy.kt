package de.hdc.tools.comp

import java.lang.reflect.*

/**
 * Creates a proxy of an Interface which you want to implement only partially.
 */
public inline fun <reified T> fakeInterface(): T =
    Proxy.newProxyInstance(
        T::class.java.classLoader,
        arrayOf(T::class.java)
    ) { _, method, _ -> error("No implementation for method $method on class ${T::class.simpleName}!") } as T

/**
 * Wraps an external library class into an internal interface.
 */
public inline fun <reified T> Any.typedAs(): T =
    Proxy.newProxyInstance(
        T::class.java.classLoader,
        arrayOf(T::class.java)
    ) { _, method, args ->
        val matchingMethod = this.javaClass.getMethod(method.name, *method.parameterTypes)
        matchingMethod.invoke(this, *(args ?: emptyArray()))
    } as T
