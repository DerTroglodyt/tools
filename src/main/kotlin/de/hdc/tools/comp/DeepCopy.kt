package de.hdc.tools.comp

import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

public fun <T : Any> T.deepCopy(): T {
    //If it is not a data class, return directly
    if (!this::class.isData) {
        return this
    }

    //Get the constructor
    return this::class.primaryConstructor!!.let { primaryConstructor ->
        primaryConstructor.parameters.associate { parameter ->
            // Conversion type
            // memberProperties returns the first of the non-extended properties and assigns the constructor to it
            // Final value=Object of the first parameter type
            val value = (this::class as KClass<T>).memberProperties.first {
                it.name == parameter.name
            }.get(this)

            // The current class here refers to the type corresponding to the parameter,
            // for example, if it is not a basic type here
            if ((parameter.type.classifier as? KClass<*>)?.isData == true) {
                parameter to value?.deepCopy()
            } else {
                parameter to value
            }

            //Finally return a new map map, that is, return a map of recombined attribute values, and call callBy to return the specified object
        }.let(primaryConstructor::callBy)
    }
}
