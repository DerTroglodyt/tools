@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import kotlinx.coroutines.sync.*

/**
 * Implements a lazy delegate that can take suspended function.
 */
@Suppress("UNCHECKED_CAST")
public fun <T> suspendLazy(
    initializer: suspend () -> T
): suspend () -> T {
    var init: (suspend () -> T)? = initializer
    val mutex = Mutex()
    var holder: T? = null

    return {
        if (init == null) holder as T
        else mutex.withLock {
            init?.let {
                holder = it()
                init = null  // prevent memory leak
            }
            holder as T
        }
    }
}
