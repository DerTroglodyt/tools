package de.hdc.tools.comp

import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.cancellation.CancellationException
import kotlin.math.pow

private fun <T, R> retryImpl(
    parm: T,
    maxRetries: UInt = 1u,
    exceptionReporter: (Exception) -> Unit = {},
    waitBeforeRetry: () -> Unit = {},
    f: (T) -> R
): R {
    var retries = maxRetries
    while (true) {
        try {
            return f(parm)
        } catch (e: Exception) {
            exceptionReporter(e)
            if (e is CancellationException || retries <= 0u) {
                throw e
            } else {
                retries -= 1u
                waitBeforeRetry()
            }
        }
    }
}

/**
 * Executes a given function and retries on an exception.
 */
public fun <T, R> retry(
    maxRetries: UInt = 1u,
    exceptionReporter: (Exception) -> Unit = {},
    waitBeforeRetry: () -> Unit = {},
    f: (T) -> R
): (T) -> R {
    return { parm: T ->
        retryImpl(parm, maxRetries, exceptionReporter, waitBeforeRetry, f)
    }
}

private suspend inline fun <T, R> retryExpImpl(
    parm: T,
    maxRetries: UInt = 1u,
    exceptionReporter: (Exception) -> Unit = {},
    backoffFactor: Double = 2.0,
    minWaitMillis: Long = 100L,
    maxWaitMillis: Long = 10_000L,
    f: (T) -> R
): R {
    var retries = 0u
    while (true) {
        try {
            return f(parm)
        } catch (e: Exception) {
            exceptionReporter(e)
            val times = backoffFactor.pow(retries.toDouble()).toInt()
            delay(minOf(maxWaitMillis, minWaitMillis * times))
            if (e is CancellationException || retries >= maxRetries) {
                throw e
            } else {
                retries += 1u
            }
        }
    }
}

/**
 * Executes a given function and retries on an exception.
 * Retries are spaced out with exponentially growing delay.
 */
public suspend fun <T, R> retryExp(
    maxRetries: UInt = 1u,
    exceptionReporter: (Exception) -> Unit = {},
    backoffFactor: Double = 2.0,
    minWaitMillis: Long = 100L,
    maxWaitMillis: Long = 10_000L,
    f: (T) -> R
): suspend (T) -> R {
    return { parm: T ->
        retryExpImpl(parm, maxRetries, exceptionReporter, backoffFactor, minWaitMillis, maxWaitMillis, f)
    }
}
