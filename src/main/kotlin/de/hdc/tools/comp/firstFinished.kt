package de.hdc.tools.comp

import kotlinx.coroutines.*
import kotlinx.coroutines.selects.select

/**
 * Starts a couple of suspending processes and awaits the result of the one that finishes first.
 *
 * *Practical example use*
 *
 *     suspend fun fetchUserData(): UserData = raceOf(
 *         { service1.fetchUserData() },
 *         { service2.fetchUserData() }
 *     )
 */
public suspend fun <T> firstFinished(
    racer: suspend CoroutineScope.() -> T,
    vararg racers: suspend CoroutineScope.() -> T
): T = coroutineScope {
    select {
        (listOf(racer) + racers).forEach { racer ->
            async { racer() }.onAwait {
                coroutineContext.job.cancelChildren()
                it
            }
        }
    }
}
