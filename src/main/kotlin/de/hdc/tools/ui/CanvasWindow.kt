package de.hdc.tools.ui

import kotlinx.coroutines.*
import java.awt.*
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.UIManager
import kotlin.math.*

internal const val TAU: Double = 2 * PI

public class CanvasWindow(name: String, private val pane: TestPane) {
    private var frame: JFrame? = null

    init {
        EventQueue.invokeLater {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
            frame = JFrame(name).apply {
                defaultCloseOperation = JFrame.EXIT_ON_CLOSE
                layout = BorderLayout()
                add(pane)
                pack()
                setLocationRelativeTo(null)
                isVisible = true
            }
        }
    }

    public fun setSize(dim: Dimension) {
        while (frame == null) {
            Thread.sleep(10)
        }
        frame?.size = dim
    }

    public fun repaint() {
        pane.repaint()
    }
}

public data class Point2D(val x: Double, val y: Double)

public open class TestPane(
    public val WIDTH: Int = 800,
    public val HEIGHT: Int = 800
) : JPanel() {
    public val W2: Int = WIDTH / 2
    public val H2: Int = HEIGHT / 2

    override fun getPreferredSize(): Dimension {
        return Dimension(WIDTH, HEIGHT)
    }

    public fun transform(p: List<Point2D>): List<Pair<Int, Int>> = p.map { W2 + it.x.toInt() to H2 + it.y.toInt() }

    public fun drawPoly(g: Graphics, p: List<Pair<Int, Int>>, closed: Boolean = true) {
        require(p.size > 1)
        for (i in 1 until p.size) {
            g.drawLine(p[i - 1].first, p[i - 1].second, p[i].first, p[i].second)
        }
        if (closed) g.drawLine(p.last().first, p.last().second, p.first().first, p.first().second)
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
    }
}
