package de.hdc.tools.math

/**
 * Ensures that a value lies in the given range (inclusive).
 * Smaller / greater values are replaced by the min/ max values of the range.
 */
public fun <T : Comparable<T>> T.clamp(min: T, max: T): T {
    return when {
        (min >= this) -> min
        (max <= this) -> max
        else -> this
    }
}

/**
 * Ensures that a value lies in the given range (inclusive).
 * Smaller / greater values are replaced by the min/ max values of the range.
 */
public fun <T : Comparable<T>> T.clamp(range: ClosedRange<T>): T =
    this.clamp(range.start, range.endInclusive)
