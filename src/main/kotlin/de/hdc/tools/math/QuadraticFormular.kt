package de.hdc.tools.math

import de.hdc.tools.types.*
import kotlin.math.*

/**
 * The two possible results of solving the quadratic equation.
 */
public sealed class RootsResult(public val result: Pair<Number, Number>)

/**
 * The two possible results of solving the quadratic equation.
 */
public class RealRoots(r: Double, s: Double) : RootsResult(r to s)

/**
 * The two possible results of solving the quadratic equation.
 */
public class ComplexRoots(r: ComplexNumber, s: ComplexNumber) : RootsResult(r to s)

/**
 * Find the roots for: ax² + bx + c
 */
public fun roots(a: Double, b: Double, c: Double): RootsResult {
    val bb = b / a
    val cc = c / a
    val m = -bb / 2
    var d = sqrt(m * m - cc)
    if (d.isNaN()) {
        d = m * m - cc
        return ComplexRoots(ComplexNumber(m, d), ComplexNumber(m, -d))
    }
    return RealRoots(m - d, m + d)
}

/**
 * Find the roots for: ax² + bx + c
 */
public fun roots(a: Int, b: Int, c: Int): RootsResult {
    return roots(a.toDouble(), b.toDouble(), c.toDouble())
}
