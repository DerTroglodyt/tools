@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.math

import java.security.*

/**
 * Calculates hash value for a String.
 *
 * Supported algorithms:
 * MD5
 * SHA-256
 * SHA-512
 */
public object HashUtils {
    /**
     * Calculates hash value for a String.
     */
    public fun sha512(input: String): String =
        hashString("SHA-512", input.toByteArray())

    /**
     * Calculates hash value for a String.
     */
    public fun sha256(input: String): String =
        hashString("SHA-256", input.toByteArray())

    /**
     * Calculates hash value for a String.
     */
    public fun md5(input: String): String =
        hashString("MD5", input.toByteArray())

    /**
     * Calculates hash value for a ByteArray.
     */
    public fun sha512(input: ByteArray): String =
        hashString("SHA-512", input)

    /**
     * Calculates hash value for a ByteArray.
     */
    public fun sha256(input: ByteArray): String =
        hashString("SHA-256", input)

    /**
     * Calculates hash value for a ByteArray.
     */
    public fun md5(input: ByteArray): String =
        hashString("MD5", input)

    /**
     * Algorithm	Supported API Levels on Android
     * MD5          1+
     * SHA-1	    1+
     * SHA-224	    1-8,22+
     * SHA-256	    1+
     * SHA-384	    1+
     * SHA-512	    1+
     */
    private fun hashString(type: String, input: ByteArray): String {
        return MessageDigest
            .getInstance(type)
            .digest(input)
            .fold("") { str, it ->
                str + "%02x".format(it)
            }
    }
}
