package de.hdc.tools.math

import kotlin.random.Random

/**
 * Picks a random item from a list.
 * All items have equal chance to get picked.
 */
public fun <T> List<T>.pick(rng: Random = Random.Default): T = this[rng.nextInt(this.size)]

/**
 * ## A list of Pairs that represent the chance to get picked and the returned item.
 * Example:
 *
 * `val list = PickList.from({5.0, "one"}, {4.0, "two"}, {1.0 "three"})`
 *
 * Chances to get picked: "one" 50%, "two" 40%, "three" 10%
 */
public class PickList<T : Any> private constructor(
    private val rng: Random = Random.Default,
    private val list: List<Pair<Double, T>>
) {
    public val count: Double = list.sumOf { it.first }

    public companion object {
        /**
         * Creates a PickList from the arguments.
         *
         * Example:
         *
         * `val list = PickList.from({5.0, "one"}, {4.0, "two"}, {1.0 "three"})`
         */
        public fun <T : Any> from(vararg items: Pair<Double, T>): PickList<T> {
            return PickList<T>(list = items.map {
                require(it.first >= 0.0)
                it
            })
        }

        /**
         * Creates a PickList from the arguments.
         *
         * Example:
         *
         * `val list = PickList.from({5.0, "one"}, {4.0, "two"}, {1.0 "three"})`
         */
        public fun <T : Any> from(rng: Random, vararg items: Pair<Double, T>): PickList<T> {
            return PickList<T>(rng, items.map {
                require(it.first >= 0.0)
                it
            })
        }
    }

    /**
     * ## Picks a random item from the list.
     *
     * All probability values are added up and a random number bewtween 0 and that sum indicates
     * the index of the item chosen.
     *
     * Example:
     *
     * `val list = listOf({5.0, "one"}, {4.0, "two"}, {1.0 "three"})`
     *
     * Chances to get picked: "one" 50%, "two" 40%, "three" 10%
     */
    public fun pick(): T {
        var x = rng.nextDouble(count)
        list.forEach { (first, second) ->
            if (x < first) {
                return second
            }
            x -= first
        }
        return list.last().second
    }

    /**
     * ## Picks a random item from the list.
     *
     * Probability is checked sequentially for each item! Probabilities __multiply__ for each element going down the list.
     * Reaching the end of the list, the last element is __always__ picked.
     *
     * __All chamce values are expected to lie in the interval 0.0..1.0!__
     *
     * Example:
     *
     * `val list = listOf({0.5, "one"}, {0.5, "two"}, {0.01 "three"})`
     *
     * Chances to get picked: "one" 50%, "two" __25%__, "three" __25%__
     */
    public fun pickFirst(): T {
        list.forEach {
            if (rng.nextDouble(0.0, 1.0) < it.first) {
                return it.second
            }
        }
        return list.last().second
    }
}
