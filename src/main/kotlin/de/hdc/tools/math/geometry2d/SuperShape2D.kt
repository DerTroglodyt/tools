package de.hdc.tools.math.geometry2d

import de.hdc.tools.ui.*
import java.awt.Color
import java.awt.Graphics
import kotlin.math.*

public class SuperShape2D : TestPane() {
    public fun superShape(angle: Double, n1: Double, n2: Double, n3: Double, m: Double, a: Double, b: Double): Point2D {
        val r = radius(angle, n1, n2, n3, m, a, b)
        val x = r * cos(angle)
        val y = r * sin(angle)
        return Point2D(x, y)
    }

    public fun radius(angle: Double, n1: Double, n2: Double, n3: Double, m: Double, a: Double, b: Double): Double {
        val part1 = abs((1.0 / a) * cos(angle * m / 4.0)).pow(n2)
        val part2 = abs((1.0 / b) * sin(angle * m / 4.0)).pow(n3)
        return 1.0 / ((part1 + part2).pow(1.0 / n1))
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        g.color = Color.BLACK
        g.fillRect(0, 0, WIDTH, HEIGHT)
        g.color = Color.WHITE

        val n1 = 1.0
        val n2 = 1.0
        val n3 = 1.0
//        val m = 5.0
        val a = 100.0
        val b = 100.0
        val increment = TAU / 500.0  // draw 500 points per shape

        for (m in 1..3) {
            val points = mutableListOf<Point2D>()
            var angle = 0.0
            do {
                points.add(superShape(angle, n1, n2, n3, m * 1.0, a, b))
                angle += increment
            } while (angle < TAU)

            drawPoly(g, transform(points))
        }
    }
}

public fun main() {
    CanvasWindow("SuperShape2D", SuperShape2D())
}
