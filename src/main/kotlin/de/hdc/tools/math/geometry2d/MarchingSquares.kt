package de.hdc.tools.math.geometry2d

import de.hdc.tools.types.*
import de.hdc.tools.ui.*
import kotlinx.coroutines.*
import java.awt.*

public class MarchingSquares : TestPane() {
    internal val field = Matrix(150, 150) { _, _ -> 0.0 }
    internal val res = 5
    internal var time = 0.0

    public fun fieldValue(x: Int, y: Int): Int = if (field[x, y] > 0) 1 else 0

    public fun drawLine(g: Graphics, a: IntPosition2D, b: IntPosition2D) {
        g.drawLine(a.x, a.y, b.x, b.y)
    }

    public fun drawBox(g: Graphics, x: Int, y: Int) {
        if ((x >= field.maxX) || (y >= field.maxY)) return

        val id = fieldValue(x, y) * 8 +
                fieldValue(x + 1, y) * 4 +
                fieldValue(x + 1, y + 1) * 2 +
                fieldValue(x, y + 1) * 1
        val a = IntPosition2D(x * res + res / 2, y * res)
        val b = IntPosition2D(x * res + res, y * res + res / 2)
        val c = IntPosition2D(x * res + res / 2, y * res + res)
        val d = IntPosition2D(x * res, y * res + res / 2)
        g.color = Color.WHITE
        when (id) {
            0 -> {}
            1 -> drawLine(g, c, d)
            2 -> drawLine(g, c, b)
            3 -> drawLine(g, d, b)
            4 -> drawLine(g, a, b)
            5 -> {
                drawLine(g, a, d)
                drawLine(g, b, c)
            }
            6 -> drawLine(g, a, c)
            7 -> drawLine(g, a, d)
            8 -> drawLine(g, a, d)
            9 -> drawLine(g, a, c)
            10 -> {
                drawLine(g, a, b)
                drawLine(g, c, d)
            }
            11 -> drawLine(g, a, b)
            12 -> drawLine(g, b, d)
            13 -> drawLine(g, b, c)
            14 -> drawLine(g, c, d)
            15 -> {}
        }
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        time += 0.003
        g.color = Color.BLACK
        g.fillRect(0, 0, WIDTH, HEIGHT)
        g.color = Color.WHITE
        val scale = 0.1
        field.pos.forEach { (x, y) ->
            field[x, y] = PerlinNoise.noise(x * scale, y * scale, time)
        }
        field.pos.forEach { (x, y) ->
            val f = (field[x, y] * 255).toInt().coerceIn(0..255)
            g.color = Color(f, 0, 0)
            g.fillRect(x * res, y * res, res, res)
            drawBox(g, x, y)
        }
    }
}

public fun main() {
    val w = CanvasWindow("Marching Squares", MarchingSquares())
    runBlocking {
        launch {
            while (true) {
                w.repaint()
                delay(10)
            }
        }
    }
}
