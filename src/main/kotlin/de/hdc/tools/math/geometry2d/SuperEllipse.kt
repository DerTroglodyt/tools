package de.hdc.tools.math.geometry2d

import de.hdc.tools.ui.*
import java.awt.Color
import java.awt.Graphics
import kotlin.math.*

public class SuperEllipse : TestPane() {
    public fun superShape(angle: Double, n: Double, a: Double, b: Double): Point2D {
        val n2 = 2.0 / n
        val x = abs(cos(angle)).pow(n2) * a * sign(cos(angle))
        val y = abs(sin(angle)).pow(n2) * b * sign(sin(angle))
        return Point2D(x, y)
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        g.color = Color.BLACK
        g.fillRect(0, 0, WIDTH, HEIGHT)
        g.color = Color.WHITE

//        val n = 2.0
        val a = 200.0
        val b = 100.0

        for (n in 3..100) {
            val points = mutableListOf<Point2D>()
            var angle = 0.0
            do {
                points.add(superShape(angle, n / 10.0, a, b))
                angle += 0.1
            } while (angle < TAU)

            drawPoly(g, transform(points))
        }
    }
}

public fun main() {
    CanvasWindow("SuperShape2D", SuperEllipse())
}
