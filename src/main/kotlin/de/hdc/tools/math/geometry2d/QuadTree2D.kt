package de.hdc.tools.math.geometry2d

import de.hdc.tools.types.*
import java.awt.*

public interface Positioned {
    public fun get(): IntPosition2D
}

public class QuadTree<T : Positioned>(
    private val bounds: IntRectangle2D,
    private val points: MutableList<T> = mutableListOf(),
    private val children: MutableList<QuadTree<T>> = mutableListOf()
) {
    public fun insert(pos: T) {
        if (!bounds.contains(pos.get())) {
            return
        }
        if (children.isEmpty() && points.size < capacity) {
            points.add(pos)
        } else {
            // subdivide
            if (children.isEmpty()) {
                val w_2 = bounds.w / 2
                val h_2 = bounds.h / 2
                children.add(QuadTree(IntRectangle2D(bounds.x, bounds.y, w_2, h_2)))
                children.add(QuadTree(IntRectangle2D(bounds.x + w_2, bounds.y, w_2, h_2)))
                children.add(QuadTree(IntRectangle2D(bounds.x, bounds.y + h_2, w_2, h_2)))
                children.add(QuadTree(IntRectangle2D(bounds.x + w_2, bounds.y + h_2, w_2, h_2)))
                points.forEach { p -> children.forEach { it.insert(p) } }
                points.clear()
            }
            // todo Optimize for less contains()
            children.forEach { it.insert(pos) }
        }
    }

    public fun remove(pos: T) {
        if (!bounds.contains(pos.get())) {
            return
        } else {
            if (points.isNotEmpty()) {
                points.remove(pos)
            } else {
                children.forEach { it.remove(pos) }
                val c = children.flatMap { it.all() }
                if (c.size <= capacity) {
                    c.forEach { points.add(it) }
                    children.clear()
                }
            }
        }
    }

    public fun query(rect: IntRectangle2D): List<T> {
        return if (bounds.intersects(rect)) {
            points.filter { rect.contains(it.get()) } + children.flatMap { it.query(rect) }
        } else {
            listOf()
        }
    }

    public fun draw(g: Graphics) {
        g.drawRect(bounds.x, bounds.y, bounds.w, bounds.h)
        points.forEach { g.drawRect(it.get().x, it.get().y, 1, 1) }
        children.forEach { it.draw(g) }
    }

    public fun forEach(action: (T) -> Unit) {
        points.forEach { action(it) }
        children.forEach { it.forEach(action) }
    }

    public fun all(): List<T> {
        return points + children.flatMap { it.all() }
    }

    internal companion object {
        const val capacity: Int = 4
    }
}
