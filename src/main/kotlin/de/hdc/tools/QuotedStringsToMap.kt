package de.hdc.tools

@Suppress("HardCodedStringLiteral")
private val SpaceNotInQuotes = Regex("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'")

/**
 * Converts a string of key="a value" pairs to a map.
 * Entries are seperated by SPACEs or new lines and values may be quoted and may contain spaces themselves.
 */
public fun propertyStringToMap(s: String): Map<String, String> {
    return SpaceNotInQuotes.findAll(s)
        .flatMap { it.value.split("=") }
        .filter { it.isNotEmpty() }
        .windowed(2, 2)
        .map { (a, b) ->
            a.trim().removeSurrounding("\"").removeSurrounding("'") to
                    b.trim().removeSurrounding("\"").removeSurrounding("'")
        }
        .toMap()
}