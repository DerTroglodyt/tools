@file:Suppress("HardCodedStringLiteral", "UNUSED_VARIABLE", "unused")

package de.hdc.tools.comp

import io.kotest.core.spec.style.*
import io.kotest.matchers.*

interface Customers {
    fun list(): List<String>
    fun add(item: String)
    fun addAll(items: List<String>)
    fun delete(item: String): Boolean
}

interface SuspendCustomers {
    suspend fun list(): List<String>
    suspend fun add(item: String)
    suspend fun addAll(items: List<String>)
    suspend fun delete(item: String): Boolean
}

/**
 * Third party class that does not know and therefor implement our interface.
 */
class ThirdPartyCustomers(
    private val list: MutableList<String> = mutableListOf()
) {
    fun list(): List<String> = list
    fun delete(item: String): Boolean = list.remove(item)

    fun add(item: String) {
        list.add(item)
    }

    fun addAll(items: List<String>) {
        list.addAll(items)
    }
}

class InterfaceProxyTest : FreeSpec({
    "Use a proxy to overlay an Interface with suspended functions" {
        val customers = object : Customers by fakeInterface() {
            override fun add(item: String) {}
        }

        val customersSuspend = object : SuspendCustomers by fakeInterface() {
            override suspend fun add(item: String) {}
        }
    }

    "wrap a third party class in an interface" {
        val thirdPartyCustomers: Customers = ThirdPartyCustomers().typedAs<Customers>()
        thirdPartyCustomers.delete("hans") shouldBe false
        thirdPartyCustomers.add("hans")
        thirdPartyCustomers.list() shouldBe listOf("hans")
        thirdPartyCustomers.delete("hans") shouldBe true
        thirdPartyCustomers.list() shouldBe emptyList()

        val items = listOf("hans", "peter", "klaus")
        thirdPartyCustomers.addAll(items)
        thirdPartyCustomers.list() shouldBe items
    }
})
