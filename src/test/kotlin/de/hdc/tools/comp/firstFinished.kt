@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

@Suppress("ClassName")
class firstFinishedTest : FreeSpec({
    "usage example" {
        suspend fun a(): String {
            delay(10)
            return "A"
        }

        suspend fun b(): String {
            delay(20)
            return "B"
        }

        suspend fun c(): String {
            delay(30)
            return "C"
        }

        coroutineScope {
            firstFinished({ c() }) shouldBe "C"
            firstFinished({ b() }, { a() }) shouldBe "A"
            firstFinished({ b() }, { c() }) shouldBe "B"
            firstFinished({ b() }, { a() }, { c() }) shouldBe "A"
        }
    }
})
