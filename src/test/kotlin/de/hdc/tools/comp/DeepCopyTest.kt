@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class DeepCopyTest : FreeSpec() {
    init {
        "a" {
            data class A(val name: String)
            data class Dep(val a: A)

            val dep = Dep(A("123"))

            val copyDep = dep.copy()
            val deepCopy = dep.deepCopy()

            (dep === copyDep) shouldBe false
            (dep === deepCopy) shouldBe false

            (dep.a === copyDep.a) shouldBe true
            (dep.a === deepCopy.a) shouldBe false
        }
    }
}
