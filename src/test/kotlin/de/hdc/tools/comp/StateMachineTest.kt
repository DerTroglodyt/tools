@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import de.hdc.tools.comp.StateMachineTest.SmEvents.*
import de.hdc.tools.comp.StateMachineTest.SmStates.*
import de.hdc.tools.comp.StateMachineTest.SmTransition.*
import de.hdc.tools.types.*
import de.hdc.tools.types.Returned.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class StateMachineTest : FreeSpec() {
    sealed class SmStates : MachineState {
        object Green : SmStates()
        object Yellow : SmStates()
        object Red : SmStates()
    }

    sealed class SmTransition(from: SmStates, to: SmStates) : MachineTransition<SmStates>(from, to) {
        object Up : SmTransition(Yellow, Green)
        object Down : SmTransition(Green, Yellow)
        object Down2 : SmTransition(Yellow, Red)
    }

    sealed class SmEvents(transition: SmTransition) : MachineEvent<SmStates, SmTransition>(transition) {
        object GoUp : SmEvents(Up)
        object GoDown : SmEvents(Down)
        object GoDown2 : SmEvents(Down2)
    }

    init {
        "init" {
            val sm = StateMachine<SmStates, SmTransition, SmEvents>(Green)
            sm.state shouldBe Green
        }

        "event with valid transition" {
            val sm = StateMachine<SmStates, SmTransition, SmEvents>(Green)
            val r = sm.on(GoDown)
            require(r.getOrNull() is StateMachine<*, *, *>)
            with(r.getOrThrow()) {
                state shouldBe Yellow
                getEvents() shouldBe listOf(GoDown)
            }
            sm.state shouldBe Green
        }

        "event with invalid transition" {
            val sm = StateMachine<SmStates, SmTransition, SmEvents>(Green)
            (sm.on(GoUp) is Failure<*, *>) shouldBe true
            sm.state shouldBe Green
        }

        "undo" {
            val sm = StateMachine<SmStates, SmTransition, SmEvents>(Green)
            (sm.undo() is Failure<*, *>) shouldBe true
            sm.state shouldBe Green

            val r = sm.on(GoDown).getOrThrow().on(GoDown2).getOrThrow()
            with(r) {
                state shouldBe Red
                getEvents() shouldBe listOf(GoDown, GoDown2)
            }

            with(r.undo().getOrThrow()) {
                state shouldBe Yellow
                getEvents() shouldBe listOf(GoDown)
            }
        }
    }
}
