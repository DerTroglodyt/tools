@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.delay

@Suppress("ClassName")
class suspendLazyTest : FreeSpec({
    "usage example" {
        var created = 0

        suspend fun makeConnection(): String {
            created += 1
            delay(10)
            return "Connection"
        }

        val getConnection = suspendLazy { makeConnection() }

        getConnection() + getConnection() + getConnection() shouldBe "ConnectionConnectionConnection"
        created shouldBe 1
    }
})
