@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class mapAsyncTest : FreeSpec ({
    "usage example" {
        class StudentsRepository {
            fun getStudentIds(): List<Int> = listOf(1, 2, 3, 7, 6, 5, 4)
            fun getStudentResult(id: Int): Int = id * 3
        }

        suspend fun getBestStudent(
            repo: StudentsRepository
        ): Int =
            repo.getStudentIds()
                .mapAsync { repo.getStudentResult(it) }
                .maxBy { it }

        getBestStudent(StudentsRepository()) shouldBe 21
    }
})
