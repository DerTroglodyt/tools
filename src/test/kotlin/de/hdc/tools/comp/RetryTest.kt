@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.comp

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.delay
import org.junit.jupiter.api.assertThrows

class RetryTest : FreeSpec({
    suspend fun succeedAfter(exceptionCount: Int): (String) -> String {
        var ec = exceptionCount
        val wrapped: (String) -> String = {
            if (ec-- == 0) it else error("deliberate")
        }
        delay(10)
        return wrapped
    }

    "retry" - {
        "not throwing" {
            val wrap = retry(f = succeedAfter(0))
            wrap("banana") shouldBe "banana"
        }

        "zero retries throws" {
            val wrap = retry(maxRetries = 0u, f = succeedAfter(1))
            assertThrows<IllegalStateException> {
                wrap("banana")
            }
        }

        "one retry should succeed" {
            val wrap = retry(f = succeedAfter(1))
            wrap("Ok") shouldBe "Ok"
        }

        "one retry should throw if two exceptions" {
            val wrap = retry(f = succeedAfter(2))
            assertThrows<IllegalStateException> {
                wrap("Ok") shouldBe "Ok"
            }
        }

        "#calls / 10 should equal #reports" {
            var x = 0
            val report: (Exception) -> Unit = {
                x += 1
            }

            val wrap = retry(maxRetries = 15u, exceptionReporter = report, f = succeedAfter(15))
            wrap("Ok")
            x shouldBe 15

            x = 0
            repeat(15) {
                val wrap2 = retry(exceptionReporter = report, f = succeedAfter(1))
                wrap2("Ok")
            }
            x shouldBe 15
        }
    }

    "retryExp" - {
        "not throwing" {
            val wrap = retryExp(f = succeedAfter(0))
            wrap("banana") shouldBe "banana"
        }

        "zero retries throws" {
            val wrap = retryExp(maxRetries = 0u, f = succeedAfter(1))
            assertThrows<IllegalStateException> {
                wrap("banana")
            }
        }

        "one retryCoop should succeed" {
            val wrap = retryExp(f = succeedAfter(1))
            wrap("Ok") shouldBe "Ok"
        }

        "one retryCoop should throw if two exceptions" {
            val wrap = retryExp(f = succeedAfter(2))
            assertThrows<IllegalStateException> {
                wrap("Ok") shouldBe "Ok"
            }
        }

        "#calls / 10 should equal #reports" {
            var x = 0
            val report: (Exception) -> Unit = {
                x += 1
            }

            val wrap = retryExp(
                maxRetries = 15u,
                maxWaitMillis = 20L,
                exceptionReporter = report,
                f = succeedAfter(15)
            )
            wrap("Ok")
            x shouldBe 15

            x = 0
            repeat(15) {
                val wrap2 = retryExp(
                    exceptionReporter = report,
                    maxWaitMillis = 20L,
                    f = succeedAfter(1)
                )
                wrap2("Ok")
            }
            x shouldBe 15
        }
    }
})
