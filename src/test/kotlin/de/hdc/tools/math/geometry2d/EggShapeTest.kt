package de.hdc.tools.math.geometry2d

import de.hdc.tools.ui.*
import io.kotest.core.spec.style.*
import kotlinx.coroutines.*
import java.awt.*

private class TPane(private val points: List<Pair<Double, Double>>) : TestPane() {
    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        g.color = Color.BLACK
        g.fillRect(0, 0, WIDTH, HEIGHT)
        g.color = Color.WHITE
        drawPoly(g, points.map {  it.first.toInt() to it.second.toInt() })
    }
}

class EggShapeTest : FreeSpec() {
    init {
        "draw some eggs" {
            val points1 = eggShell(
                400,
                400.0,
                300.0,
                0.0,
                200.0
            )
            val points2 = eggShell(
                400,
                400.0,
                300.0,
                40.0,
                180.0
            )
            val points3 = eggShell(
                400,
                400.0,
                300.0,
                60.0,
                160.0
            )
            val points4 = eggShell(
                400,
                400.0,
                400.0,
                0.0,
                330.0
            )
            val win = CanvasWindow("SuperShape2D", TPane(points1 + points2 + points3 + points4))
            win.repaint()
            withContext(Dispatchers.IO) {
                Thread.sleep(1000)
            }
        }
    }
}
