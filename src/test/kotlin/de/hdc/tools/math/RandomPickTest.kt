@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.math

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.collections.shouldBeOneOf
import io.kotest.matchers.doubles.plusOrMinus
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.shouldBe

class RandomPickTest : FreeSpec() {
    init {
        "Pick from List" {
            val list = (1..1000).map { it.toString() }
            val picked1 = list.pick()
            picked1 shouldBeOneOf list

            val picked2 = list.pick()
            picked2 shouldBeOneOf list

            picked1 shouldNotBe picked2
        }

        "Pick from Pairs" {
            val list = listOf(1.0 to "One", 2.0 to "Two", 3.0 to "Three", 4.0 to "Four", 1.0 to "Five")
            val pl = PickList.from(*list.toTypedArray())
            val r = list.associate { it.second to 0 }.toMutableMap()
            val max = 1_000_000
            (1..max).forEach {
                val p = pl.pick()
                r[p] = r[p]!! + 1
            }
            val maxDiv = 0.025
            println(r)
            list.forEach { (v, s) ->
                (r[s]!!.toDouble() / (max.toDouble() / pl.count)).shouldBe(v plusOrMinus maxDiv)
            }
        }

        "PickFirst from Pairs" {
            val list = listOf(0.5 to "One", 0.5 to "Two", 0.5 to "Three", 0.9 to "Four")
            val pl = PickList.from(*list.toTypedArray())
            val r = list.associate { it.second to 0 }.toMutableMap()
            val max = 1_000_000
            (1..max).forEach {
                val p = pl.pickFirst()
                r[p] = r[p]!! + 1
            }
            val maxDiv = 0.025
            r["One"]?.div(max * list[0].first).shouldBe(1.0 plusOrMinus maxDiv)
            r["Two"]?.div(max * list[0].first * list[1].first).shouldBe(1.0 plusOrMinus maxDiv)
            r["Three"]?.div(max * list[0].first * list[1].first * list[2].first).shouldBe(1.0 plusOrMinus maxDiv)
            val rest = max -
                    max * list[0].first -
                    max * list[0].first * list[1].first -
                    max * list[0].first * list[1].first * list[2].first
            r["Four"]?.div(rest)
                .shouldBe(1.0 plusOrMinus maxDiv)
        }
    }
}
