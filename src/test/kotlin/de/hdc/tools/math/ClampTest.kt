package de.hdc.math

import de.hdc.tools.math.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class ClampTest : FreeSpec({
    "some clamping" {
        0.clamp(1, 5) shouldBe 1
        0.clamp(1..5) shouldBe 1

        0.0.clamp(1.0, 5.0) shouldBe 1.0
        0.0.clamp(1.0..5.0) shouldBe 1.0
    }
})
