@file:Suppress("HardCodedStringLiteral")
@file:OptIn(ExperimentalStdlibApi::class)

package de.hdc.tools.math

import de.hdc.tools.*
import de.hdc.tools.math.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import java.io.*
import java.util.logging.*

@Suppress("unused")
class HashUtilsTest : FreeSpec() {

    init {
        useNiceLogging(Level.FINEST)

        "sha512" {
            readTest("SHA512LongMsg.rsp").forEach { (msg, md) ->
                HashUtils.sha512(msg.hexToByteArray()) shouldBe md
            }
            readTest("SHA512ShortMsg.rsp").forEach { (msg, md) ->
                HashUtils.sha512(msg.hexToByteArray()) shouldBe md
            }
        }

        "sha256" {
            readTest("SHA256LongMsg.rsp").forEach { (msg, md) ->
                HashUtils.sha256(msg.hexToByteArray()) shouldBe md
            }
            readTest("SHA256ShortMsg.rsp").forEach { (msg, md) ->
                HashUtils.sha256(msg.hexToByteArray()) shouldBe md
            }
        }
    }

    private fun readTest(fileName: String): List<Pair<String, String>> {
        try {
            val stream = Thread.currentThread().contextClassLoader.getResourceAsStream(fileName)
                ?: throw IOException("Could not read resource file $fileName!")
            val list = mutableListOf<Pair<String, String>>()
            var msg = ""
            stream.buffered().reader().useLines { lines ->
                lines.dropWhile { row -> row.startsWith('#') }
                    .filterNot { it.isEmpty() }
                    .map { row ->
                        val items = row.split(" ", limit = 5)
                        if (items[0] == "Msg") {
                            msg = items[2]
                        }
                        if (items[0] == "MD") {
                            list.add(msg to items[2])
                        }
                    }
                    .count()
            }
            return list
        } catch (e: Exception) {
            logError { "Could not create input stream for file $fileName! $e" }
            error { "Could not create input stream for file $fileName! $e" }
        }
    }

}
