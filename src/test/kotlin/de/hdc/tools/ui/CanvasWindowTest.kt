@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.ui

import io.kotest.core.spec.style.*
import kotlinx.coroutines.*
import java.awt.*

class CanvasWindowTest : FreeSpec({
    "resize window" {
        val w = CanvasWindow("Test", TestPane())
        w.setSize(Dimension(1024, 800))
        delay(1000)
    }
})
