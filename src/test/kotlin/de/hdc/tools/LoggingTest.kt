@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

import io.kotest.core.spec.style.*
import io.kotest.matchers.*

@Suppress("unused")
class TestClass {
    fun error() {
        logError { "Some intentional error." }
    }

    companion object {
        fun someGlitch() {
            logWarning { "Some intentional warning." }
        }
    }
}

class LoggingTest : FreeSpec({
    "companion should log as parent class name" {
        getClassName(TestClass().javaClass) shouldBe "de.hdc.tools.TestClass"
        getClassName(TestClass.Companion::class.java) shouldBe "de.hdc.tools.TestClass\$Companion"
    }
})
