package de.hdc.tools.types

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import java.io.IOException

@Suppress("HardCodedStringLiteral")
class ReturnedTest : FreeSpec({
    "create success" {
        var or = ""
        val r = Returned.success("success")
        r.getOrNull() shouldBe "success"
        r.failureOrNull() shouldBe null
        r.getOrThrow() shouldBe "success"
        r.onSuccess { or = "success" }
        or shouldBe "success"
        r.onFailure { or = "failure" }
        or shouldBe "success"
    }

    "create success with null" {
        var or = ""
        val r = Returned.success(null)
        r.getOrNull() shouldBe null
        r.failureOrNull() shouldBe null
        r.getOrThrow() shouldBe null
        r.onSuccess { or = "success" }
        or shouldBe "success"
        r.onFailure { or = "failure" }
        or shouldBe "success"
    }

    "failure" {
        var or = ""
        val r = Returned.failure("failure")
        r.getOrNull() shouldBe null
        r.failureOrNull() shouldBe "failure"
        r.onSuccess { or = "success" }
        or shouldBe ""
        r.onFailure { or = "failure" }
        or shouldBe "failure"
    }

    "throw exception" {
        Returned.success("success")
            .getOrThrow() shouldBe "success"

        val r: Returned<Any?, IOException> = Returned.failure(IOException("failure"))
        shouldThrow<IOException> { r.getOrThrow() }
    }

    "fold" {
        val rs = Returned.success("success")
        val rrs = rs.fold(
            { "success" },
            { "failure" }
        )
        rrs shouldBe "success"

        val rf = Returned.failure("failure")
        val rrf = rf.fold(
            { "success" },
            { "failure" }
        )
        rrf shouldBe "failure"
    }
})
