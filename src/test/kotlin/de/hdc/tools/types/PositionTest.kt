@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.types

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class PositionTest : FreeSpec({
    "neighbours" {
        val n5 = IntPosition2D(1, 1).neighboursOrthoDiag()
        n5.size shouldBe 8
        n5.forEach {
            (it in listOf(
                IntPosition2D(0, 0),
                IntPosition2D(1, 0),
                IntPosition2D(2, 0),
                IntPosition2D(0, 1),
                IntPosition2D(1, 1),
                IntPosition2D(2, 1),
                IntPosition2D(0, 2),
                IntPosition2D(1, 2),
                IntPosition2D(2, 2)
            )) shouldBe true
        }
    }
})
