package de.hdc.tools.types

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class RangedNumberTest : FreeSpec({
    "delegate" {
        val d: Double by RangedDouble(0.0, 0.0..100.0)
        d shouldBe 0.0

        val d2: Double by RangedDouble(-10.0, 0.0..100.0)
        d2 shouldBe 0.0
//        d2 += 2.0  // Should show compiler error

        var d3: Double by MutableRangedDouble(-10.0, 0.0..100.0)
        d3 += 20.0
        d3 shouldBe 20.0
        d3 = 50.0
        d3 shouldBe 50.0
        d3 -= 150.0
        d3 shouldBe 0.0
    }

    "init values" {
        val r = RangedDouble(4.0, 0.0..1.0)
        r.value shouldBe 1.0
        r.range shouldBe 0.0..1.0
    }

    "math" {
        val range = 1.0..3.0
        val r2 = RangedDouble(2.0, range)
        val r3 = RangedDouble(3.0, range)

        val r4 = RangedDouble(3.0, -10.0..+10.0)
        (-r4).value shouldBe -3.0
        (+r4).value shouldBe 3.0

        (r2 + r3).value shouldBe 3.0
        (r2 - r3).value shouldBe 1.0
        (r2 * r3).value shouldBe 3.0
        (r2 / r3).value shouldBe 1.0
        (r2 % r2).value shouldBe 1.0

        (r2 + 3).value shouldBe 3.0
        (r2 - 3).value shouldBe 1.0
        (r2 * 3).value shouldBe 3.0
        (r2 / 3).value shouldBe 1.0
        (r2 % 2).value shouldBe 1.0
    }

    "compare" {
        val r = RangedDouble(4.0, 1.0..5.0)
        val r2 = RangedDouble(2.0, 1.0..5.0)

        r shouldBe r
        r.value shouldBe r.value
        r.range shouldBe r.range
        r.range shouldBe r2.range

        (r > r2) shouldBe true
        (r < r2) shouldBe false
        (r2 > r) shouldBe false
        (r2 < r) shouldBe true
    }
})
