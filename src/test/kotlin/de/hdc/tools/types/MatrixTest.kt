@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.types

import io.kotest.core.spec.style.*

class MatrixTest : FreeSpec() {
    val test = """
                5483143223
                2745854711
                5264556173
                6141336146
                6357385478
                4167524645
                2176841721
                6882881134
            """.trimIndent().split("\n")

    init {
// TODO
//        "func" {
//            val m = Matrix(test) { s: String -> s.map { it.digitToInt() }.toMutableList() }
//            val m2 = Matrix(test) { s: String -> s.map { it.digitToInt() }.toMutableList() }
//            val m3 = Matrix(test) { s: String -> s.reversed().map { it.digitToInt() }.toMutableList() }
//            (m == m2) shouldBe true
//            (m == m3) shouldBe false
//            m.row(1) shouldBe listOf(2, 7, 4, 5, 8, 5, 4, 7, 1, 1)
//            m.pos.take(3) shouldBe listOf(IntPosition2D(0, 0), IntPosition2D(1, 0), IntPosition2D(2, 0))
//            m.columnIndices() shouldBe 0..9
//            m.rowIndices() shouldBe 0..7
//        }

//        "neighbours" {
//            val m = Matrix(test) { s: String -> s.map { it.digitToInt() }.toMutableList() }
//            val n1 = m.neighboursOrthoDiag(IntPosition2D(0, 0))
//            n1.size shouldBe 3
//            n1.forEach {
//                (it in listOf(
//                    IntPosition2D(1, 0),
//                    IntPosition2D(0, 1),
//                    IntPosition2D(1, 1)
//                )) shouldBe true
//            }
//
//            val n2 = m.neighboursOrthoDiag(IntPosition2D(9, 9))
//            n2.size shouldBe 3
//            n2.forEach {
//                (it in listOf(
//                    IntPosition2D(8, 9),
//                    IntPosition2D(9, 8),
//                    IntPosition2D(8, 8)
//                )) shouldBe true
//            }
//
//            val n3 = m.neighboursOrthoDiag(IntPosition2D(0, 9))
//            n3.size shouldBe 3
//            n3.forEach {
//                (it in listOf(
//                    IntPosition2D(1, 9),
//                    IntPosition2D(0, 8),
//                    IntPosition2D(1, 8)
//                )) shouldBe true
//            }
//
//            val n4 = m.neighboursOrthoDiag(IntPosition2D(9, 0))
//            n4.size shouldBe 3
//            n4.forEach {
//                (it in listOf(
//                    IntPosition2D(8, 0),
//                    IntPosition2D(9, 1),
//                    IntPosition2D(8, 1)
//                )) shouldBe true
//            }
//
//            val n5 = m.neighboursOrthoDiag(IntPosition2D(1, 1))
//            n5.size shouldBe 8
//            n5.forEach {
//                (it in listOf(
//                    IntPosition2D(0, 0),
//                    IntPosition2D(1, 0),
//                    IntPosition2D(2, 0),
//                    IntPosition2D(0, 1),
//                    IntPosition2D(1, 1),
//                    IntPosition2D(2, 1),
//                    IntPosition2D(0, 2),
//                    IntPosition2D(1, 2),
//                    IntPosition2D(2, 2)
//                )) shouldBe true
//            }
//
//            val n6 = m.neighboursOrthoDiag(IntPosition2D(1, 0))
//            n6.size shouldBe 5
//            n6.forEach {
//                (it in listOf(
//                    IntPosition2D(0, 0),
//                    IntPosition2D(2, 0),
//                    IntPosition2D(0, 1),
//                    IntPosition2D(1, 1),
//                    IntPosition2D(2, 1),
//                )) shouldBe true
//            }
//        }
    }
}
