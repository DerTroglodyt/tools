@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.types

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class PolynomTest : FreeSpec({
    "toString" {
        val p = Polynom(mapOf(2u to 1.0, 1u to 3.0, 0u to 5.0))
        p.toString() shouldBe "Polynom(1.0x^2 + 3.0x^1 + 5.0x^0)"
        p.derivative.toString() shouldBe "Polynom(2.0x^1 + 3.0x^0)"
    }

    "derivativeAt" {
        val p = Polynom(mapOf(2u to 1.0, 1u to 3.0, 0u to 5.0))
        p.derivativeAt(2.0) shouldBe 7.0
    }
})
