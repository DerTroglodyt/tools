@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.types

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.json.Json

class IdTest : FreeSpec() {
    init {
        "Returned strings are all of specified length." {
            (0 until 10).map {
                Id<Any>().toString().length shouldBe (32 + 4)
            }
        }

        "Returned strings are unique." {
            val id = Id<Any>()
            (0 until 10).map { Id<Any>() shouldNotBe id }
        }

        "serialisation" {
            val id1 = Id<Double>()
            val json = Json.encodeToString(Id.serializer(Double.serializer()), id1)

            val id2 = Json.decodeFromString<Id<Double>>(json)
            id1 shouldBe id2
        }
    }
}
