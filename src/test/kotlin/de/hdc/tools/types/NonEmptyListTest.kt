@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools.types

import de.hdc.tools.types.*
import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class NonEmptyListTest : FreeSpec({
    "create from List" {
        val list = listOf("a", "b", "c").toNonEmptyList()
        list.value.size shouldBe 3
    }

    "trying to create an empty list fails" {
        shouldThrow<IllegalArgumentException> {
            listOf<String>().toNonEmptyList()
        }

        shouldThrow<IllegalArgumentException> {
            NonEmptyList(emptyList<String>())
        }

        shouldThrow<IllegalArgumentException> {
            NonEmptyList(listOf<String>())
        }

        shouldThrow<IllegalArgumentException> {
            NonEmptyList<String>()
        }
    }
})
