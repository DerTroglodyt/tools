@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class HexUtilsTest : FreeSpec() {
    init {
        "invalid hex strings" {
            shouldThrow<IllegalArgumentException> {
                HexString("")
            }
            shouldThrow<IllegalArgumentException> {
                HexString("fx")
            }
            shouldThrow<IllegalArgumentException> {
                HexString("01fax")
            }
        }

        "ByteArray to String" {
            byteArrayOf(0).toHexString() shouldBe "00"
            byteArrayOf(1).toHexString() shouldBe "01"
            byteArrayOf(-1).toHexString() shouldBe "ff"
            byteArrayOf(1, 2, 3, 4, 5).toHexString() shouldBe "01 0203 0405"
        }

        "String to ByteArray" {
            HexString("00 01ff").toByteArray() shouldBe byteArrayOf(0, 1, -1)
            HexString("00  01ff").toByteArray() shouldBe byteArrayOf(0, 1, -1)
            HexString("00-01ff").toByteArray() shouldBe byteArrayOf(0, 1, -1)
            HexString("00:01ff").toByteArray() shouldBe byteArrayOf(0, 1, -1)
        }
    }
}
