@file:Suppress("HardCodedStringLiteral")

package de.hdc.tools

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class QuotedStringsToMapTest : FreeSpec() {
    init {
        "empty string" {
            propertyStringToMap("").isEmpty() shouldBe true
        }

        "one pair" {
            val m = propertyStringToMap("  Hans='fertig'   ")
            m["Hans"] shouldBe "fertig"
        }

        "ignore whitespaces" {
            val m = propertyStringToMap("  Hans='fertig'   ")
            m["Hans"] shouldBe "fertig"
        }

        "multiple pairs 1" {
            val m = propertyStringToMap("  Hans=\"fertig\"    on='off'   Peter=\"faul\"   ")
            m["Hans"] shouldBe "fertig"
            m["on"] shouldBe "off"
            m["Peter"] shouldBe "faul"
        }

        "multiple pairs 2" {
            val m = propertyStringToMap("  Hans=fertig    on=off   Peter=faul   ")
            m["Hans"] shouldBe "fertig"
            m["on"] shouldBe "off"
            m["Peter"] shouldBe "faul"
        }

        "multiple pairs 3" {
            val m = propertyStringToMap("  Hans=\"fertig\"    on=off   Peter=\"faul\"   ")
            m["Hans"] shouldBe "fertig"
            m["on"] shouldBe "off"
            m["Peter"] shouldBe "faul"
        }

        "multiple pairs 4" {
            val m = propertyStringToMap("  Hans=\"fertig\"\non=off\nPeter=\"faul\"")
            m["Hans"] shouldBe "fertig"
            m["on"] shouldBe "off"
            m["Peter"] shouldBe "faul"
        }

        "multiple pairs 5" {
            val m = propertyStringToMap("  Hans=\"fertig\"\non=off\nPeter=\"faul\"\n")
            m["Hans"] shouldBe "fertig"
            m["on"] shouldBe "off"
            m["Peter"] shouldBe "faul"
        }
    }
}